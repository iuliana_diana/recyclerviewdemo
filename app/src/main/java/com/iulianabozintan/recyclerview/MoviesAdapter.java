package com.iulianabozintan.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ViewHolder> {
    private ArrayList<Movie> mMovies;
    private OnItemClickListener mOnItemClickListener;

    MoviesAdapter(ArrayList<Movie> movies, OnItemClickListener onItemClickListener) {
        mMovies = movies;
        mOnItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_movie_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData();
    }

    @Override
    public int getItemCount() {
        if (mMovies == null) {
            return 0;
        }
        return mMovies.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mMovieTitle;
        private TextView mMovieYear;

        ViewHolder(View itemView) {
            super(itemView);

            mMovieTitle = itemView.findViewById(R.id.tv_movie_title);
            mMovieYear = itemView.findViewById(R.id.tv_movie_year);
            itemView.setOnClickListener(this);
        }

        void bindData() {
            Movie movie = mMovies.get(getAdapterPosition());
            mMovieTitle.setText(movie.getName());
            mMovieYear.setText(String.valueOf(movie.getReleaseYear()));
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(getAdapterPosition(), mMovies.get(getAdapterPosition()));
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position, Movie movie);
    }
}

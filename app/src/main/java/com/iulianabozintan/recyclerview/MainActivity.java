package com.iulianabozintan.recyclerview;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MoviesAdapter.OnItemClickListener {
    private static final int NUMBER_OF_ITEMS = 50;
    private ArrayList<Movie> mMovies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViewsForMovies();
    }

    private void initViewsForNumbers() {
        RecyclerView recyclerView = findViewById(R.id.rv_numbers);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        NumbersAdapter adapter = new NumbersAdapter(NUMBER_OF_ITEMS);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void initViewsForMovies() {
        generateList();
        RecyclerView recyclerView = findViewById(R.id.rv_numbers);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        MoviesAdapter adapter = new MoviesAdapter(mMovies, this);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void generateList() {
        mMovies = new ArrayList<>();
        mMovies.add(new Movie("The Shawshank Redemption", 1994));
        mMovies.add(new Movie("The Godfather", 1972));
        mMovies.add(new Movie("The Godfather: Part II", 1974));
        mMovies.add(new Movie("The Dark Knight", 2008));
        mMovies.add(new Movie("12 Angry Men", 1957));
        mMovies.add(new Movie("Schindler's List", 1993));
        mMovies.add(new Movie("The Lord of the Rings: The Return of the King", 2003));
        mMovies.add(new Movie("Pulp Fiction", 1994));
        mMovies.add(new Movie("Il buono, il brutto,, il cattivo", 1966));
        mMovies.add(new Movie("Fight Club", 1999));
        mMovies.add(new Movie("The Lord of the Rings: The Fellowship of the Ring", 2001));
        mMovies.add(new Movie("Forrest Gump", 1994));
        mMovies.add(new Movie("Star Wars: Episode V - The Empire Strikes Back", 1980));
        mMovies.add(new Movie("Inception", 2010));
        mMovies.add(new Movie("One Flew Over the Cuckoo's Nest", 1975));
        mMovies.add(new Movie("The Lord of the Rings: The Two Towers", 2002));
        mMovies.add(new Movie("The Lord of the Rings: The Two Towers", 2002));
        mMovies.add(new Movie("The Lord of the Rings: The Two Towers", 2002));
        mMovies.add(new Movie("The Lord of the Rings: The Two Towers", 2002));
        mMovies.add(new Movie("The Lord of the Rings: The Two Towers", 2002));
        mMovies.add(new Movie("The Lord of the Rings: The Two Towers", 2002));
        mMovies.add(new Movie("The Lord of the Rings: The Two Towers", 2002));
        mMovies.add(new Movie("The Lord of the Rings: The Two Towers", 2002));
        mMovies.add(new Movie("The Lord of the Rings: The Two Towers", 2002));
        mMovies.add(new Movie("The Lord of the Rings: The Two Towers", 2002));
        mMovies.add(new Movie("The Lord of the Rings: The Two Towers", 2002));
        mMovies.add(new Movie("The Lord of the Rings: The Two Towers", 2002));
    }

    @Override
    public void onItemClick(int position, Movie movie) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(movie.getName())
                .setMessage("Clicked on position " + position
                        + "\nMovie release year: " + movie.getReleaseYear())
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        // Create the AlertDialog object and return it
        builder.create().show();
    }
}

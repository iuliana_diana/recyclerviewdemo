package com.iulianabozintan.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class NumbersAdapter extends RecyclerView.Adapter<NumbersAdapter.ViewHolder> {
    private static final int VIEW_TYPE_LEFT = 0;
    private static final int VIEW_TYPE_RIGHT = 1;

    private static final String TAG = NumbersAdapter.class.getSimpleName();
    private static int sViewHolderCount;
    private int mNumberOfItems;

    public NumbersAdapter(int numberOfItems) {
        mNumberOfItems = numberOfItems;
        sViewHolderCount = 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 0) {
            return VIEW_TYPE_LEFT;
        } else {
            return VIEW_TYPE_RIGHT;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEW_TYPE_LEFT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_number_item_left, parent, false);
                break;
            case VIEW_TYPE_RIGHT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_number_item_right, parent, false);
                break;
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_number_item_left, parent, false);
                break;
        }
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.mHolderPosition.setText("ViewHolder: " + sViewHolderCount);
        int backgroundColorForViewHolder = ColorUtils
                .getViewHolderBackgroundColorFromInstance(parent.getContext(), sViewHolderCount);
        viewHolder.itemView.setBackgroundColor(backgroundColorForViewHolder);
        sViewHolderCount++;
        Log.d(TAG, "onCreateViewHolder: number of ViewHolders created: "
                + sViewHolderCount);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData();
    }

    @Override
    public int getItemCount() {
        return mNumberOfItems;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mItemPosition;
        TextView mHolderPosition;

        ViewHolder(View itemView) {
            super(itemView);

            mItemPosition = itemView.findViewById(R.id.tv_item_position);
            mHolderPosition = itemView.findViewById(R.id.tv_holder_position);
        }

        void bindData() {
            mItemPosition.setText(String.valueOf(getAdapterPosition()));
        }
    }
}
